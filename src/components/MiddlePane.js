import React from 'react';
import Grid from './Grid'

function MiddlePane() {
    return (
        <Grid item xs>Middle</Grid>
    )
}

export default MiddlePane;