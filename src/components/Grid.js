import MuiGrid from '@mui/material/Grid';
import { styled } from '@mui/system';


const Grid = styled(MuiGrid)(({theme}) => ({
  width:'100%',
  '& [role="separator"]': {
      margin: theme.spacing(0, 2),
  },
}));

export default Grid;