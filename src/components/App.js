import { Divider } from '@mui/material';

import React from 'react';
import './App.css';
import RightPane from './RightPane';
import LeftPane from './LeftPane';
import MiddlePane from './MiddlePane';
import Grid from './Grid'


function App() {
  return (
    <Grid container>
      <LeftPane />
      <Divider orientation="vertical" variant="middle"/>
      <MiddlePane />
      <Divider orientation="vertical" />
      <RightPane />
    </Grid>

  );
}

export default App;
